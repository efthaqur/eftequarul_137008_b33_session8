<?php
/**
 *
 * Prime number Printing
 * Date: 10/3/2016
 * Time: 12:07 PM
 */

    $start=1;
    $end=100;

    for($num=$start; $num<=$end; $num++){
        if($num==1) continue;
        $isPrime=true;
        for($i=2; $i<=ceil($num/2); $i++){
            if($num%$i==0){
                $isPrime=false;
                break;
            }
        } // End of prime checking loop
        if($isPrime) echo "Prime number:".$num."<br>";
    }